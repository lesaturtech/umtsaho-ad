const state = () => ({
    categorie: {
        code: '',
        nameEn: '',
        nameFr: '',
        descriptionEn: '',
        descriptionFr: '',
        price: 0
    }
})

const mutations = {
    storeCode: (state, data) => {
        state.categorie.code = data
    },
    storeNameEn: (state, data) => {
        state.categorie.nameEn = data
    },
    storeNameFr: (state, data) => {
        state.categorie.nameFr = data
    },
    storeDescriptionEn: (state, data) => {
        state.categorie.descriptionEn = data
    },
    storeDescriptionFr: (state, data) => {
        state.categorie.descriptionFr = data
    },
    storePrice: (state, data) => {
        state.categorie.price = data
    }
}

export default {
    state,
    mutations
}