import Promote from "~/plugins/api/promote";
import Categorie from "~/plugins/api/categorie";

export default (context, inject) => {
    context.$axios.setHeader('Accept', 'application/json')
    context.$axios.onRequest((config) => {
        console.log(`Request Config: ${config.url} ---`)
        console.log(config)
    })

    context.$axios.onError((error) => {
        const code = parseInt(error.response && error.response.status)
        console.log(`Error ${code}`)
        console.error({ error })
    })

    // Initialize API factories
    const factories = {
        promote: Promote(context.$axios),
        categorie: Categorie(context.$axios),
    };

    // Inject $api
    inject("api", factories);
};
