export default axios => ({
    create(categorie) {
        console.log(categorie);
        return axios.post("categories", categorie);
    },
    get() {
        return axios.get("categories");
    },
    update(target) {
        return axios.put("categorie/"+target.key, {description:target.description});
    },
    delete(id) {
        return axios.delete("categorie/"+id);
    },
});
